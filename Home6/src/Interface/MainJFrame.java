/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.*;
import java.awt.CardLayout;
import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author user
 */
public class MainJFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainJFrame
     */
    private PersonList salesRepList;
    private MarketList marketList;
    private CustomerList customerList;
    private SupplierList supplierList;
    private OrderList ordersList;
    private ProductList productList;
    private PersonList personList;
    private Person person;

    public MainJFrame() {

        salesRepList = new PersonList();
        customerList = new CustomerList();
        marketList = new MarketList();
        supplierList = new SupplierList();
        ordersList = new OrderList();
        productList = new ProductList();
        personList = new PersonList();

        initComponents();
        setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
        Business business = ConfigureABusiness.BusinessInitialize();
        Customer c = business.getCustomer();
        Market m = business.getMarket();
        Person p = business.getSalesRep();
        Order o = new Order();

        /* CSV IMPORTS OF DATA */
        // Market List import
        String line = "";
        String separated = ",";
        String fileName = "MockData/MockData-Market.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] data = line.split(separated);
                if (data[0].equalsIgnoreCase("Serial No")) {
                    continue;
                }
                String marketSr = data[0];
                String marketName = data[1];
                m = marketList.addMarket();
                m.setSerialNo(marketSr);
                m.setMarket(marketName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Supplier List Import
        Supplier supplier = m.getSupplier();
        fileName = "MockData/MockData-Suppliers.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] data = line.split(separated);
                if (data[0].equals("Serial No")) {
                    continue;
                }
                String supplierSr = data[0];
                String supplierName = data[1];
                String suppMarketName = data[2];
                String username = data[3];
                String password = data[4];
                supplier = supplierList.addSupplier();
                supplier.setSerialNo(supplierSr);
                supplier.setSupplierName(supplierName);
                supplier.setMarketName(suppMarketName);
                // Add the supplier as a person
                p = personList.addSalesRep();
                p.setPersonName(supplierName);
                p.setPersonUsername(username);
                try {
                    p.setPersonPassword(PasswordEncrypt.PasswordEncrypt(password));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                p.setPersonRole("supplier");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Product list import
        Product product = supplier.getProduct();
        fileName = "MockData/MockData-Products.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] data = line.split(separated);
                String productSr = data[0];
                String productName = data[1];
                if (data[0].equalsIgnoreCase("Sr No")) {
                    continue;
                }
                int floorVal = Integer.parseInt(data[2]);
                int targetVal = Integer.parseInt(data[3]);
                int ceilingVal = Integer.parseInt(data[4]);
                String supplierName = data[5];
                String marketName = data[6];
                product = productList.addProduct();
                product.setProductID(productSr);
                product.setProductName(productName);
                product.setFloorVal(floorVal);
                product.setActualVal(targetVal);
                product.setCeilVal(ceilingVal);
                product.setSupplierName(supplierName);
                
                for(Market mar : marketList.getMarketList()) {
                    if(mar.getMarket().equalsIgnoreCase(marketName)) {
                        product.setMarket(mar);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Customer list import
        fileName = "MockData/MockData-Customers.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] data = line.split(separated);
                String customerName = data[0];
                String customerAge = data[1];
                String customerGender = data[2];
                String customerMarket = data[3];
                c = customerList.addCustomer();
                c.setCustomerName(customerName);
                c.setCustomerAge(customerAge);
                c.setCustomerGender(customerGender);
                c.setCustomerMarketType(customerMarket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Orders list import
        fileName = "MockData/MockData-Orders.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] data = line.split(separated);
                if (data[0].equalsIgnoreCase("Order ID")) {
                    continue;
                }
                int orderID = Integer.parseInt(data[0]);
                String productName = data[1];
                String customerName = data[2];
                int unitCost = Integer.parseInt(data[3]);
                int quantity = Integer.parseInt(data[4]);
                String salesRepName = data[5];
                o = ordersList.addOrder();

                o.setOrderID(orderID);
                o.setSalesRepName(salesRepName);
                o.setOrderAmount(unitCost);
                o.setOrderQuantity(quantity);

                for (Product prod : productList.getProductList()) {
                    if (prod.getProductName().equals(productName)) {
                        o.setProduct(prod);
                    }
                }

                for (Customer cust : customerList.getCustomerList()) {
                    if (cust.getCustomerName().equals(customerName)) {
                        o.setCustomer(cust);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        // Sales Rep list import
        fileName = "MockData/MockData-SalesRep.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            while ((line = br.readLine()) != null) {
                String[] data = line.split(separated);
                if (data[0].equalsIgnoreCase("Name")) {
                    continue;
                }
                String personName = data[0];
                String personUsername = data[1];
                String personPassword = data[2];
                p = personList.addSalesRep();
                p.setPersonName(personName.trim());
                p.setPersonUsername(personUsername.trim());
                p.setPersonPassword(personPassword.trim());
                p.setPersonRole("sales rep");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        LoginMain panel = new LoginMain(mainProcessContainer, business, c, marketList, salesRepList, customerList, supplierList, ordersList, productList, personList, person);

        mainProcessContainer.add("LoginMain", panel);
        CardLayout layout = (CardLayout) mainProcessContainer.getLayout();
        layout.next(mainProcessContainer);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainProcessContainer = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        mainProcessContainer.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainProcessContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 1275, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mainProcessContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 675, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel mainProcessContainer;
    // End of variables declaration//GEN-END:variables
}
