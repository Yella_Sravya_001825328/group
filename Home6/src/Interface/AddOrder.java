/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.*;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author user
 */
public class AddOrder extends javax.swing.JPanel {

    private JPanel mainProcessContainer;
    private Business business;
    private OrderList ordersList;
    private ProductList productList;
    private CustomerList customerList;
    private Product product;

   
    public AddOrder(JPanel mainProcessContainer, Business business, OrderList ordersList, ProductList productList, Supplier supplier, CustomerList customerList) {
        initComponents();
        this.mainProcessContainer = mainProcessContainer;
        this.business = business;
        this.ordersList = ordersList;
        this.productList = productList;
        this.customerList = customerList;
        

        for (Product p : productList.getProductList()) {
            productDrop.addItem(p.getProductName());
        }

        for (Customer c : customerList.getCustomerList()) {
            customerDrop.addItem(c.getCustomerName());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        productDrop = new javax.swing.JComboBox<>();
        saleAmount = new javax.swing.JTextField();
        saveOrder = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        customerDrop = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        saleQuantity = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        jLabel2.setText("PRODUCT");

        jLabel3.setText("SALE AMOUNT");

        productDrop.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECT --" }));
        productDrop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                productDropActionPerformed(evt);
            }
        });

        saleAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                saleAmountKeyTyped(evt);
            }
        });

        saveOrder.setText("ADD ORDER");
        saveOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveOrderActionPerformed(evt);
            }
        });

        jLabel4.setText("CUSTOMER");

        customerDrop.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECT --" }));
        customerDrop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customerDropActionPerformed(evt);
            }
        });

        jLabel5.setText("QUANTITY");

        saleQuantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saleQuantityActionPerformed(evt);
            }
        });
        saleQuantity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                saleQuantityKeyTyped(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jButton3.setText("<< BACK");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel1.setText("Add Order");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(122, 122, 122)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton3)
                        .addComponent(jLabel5)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(saveOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(saleQuantity, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(productDrop, javax.swing.GroupLayout.Alignment.LEADING, 0, 146, Short.MAX_VALUE)
                        .addComponent(customerDrop, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(saleAmount, javax.swing.GroupLayout.Alignment.LEADING)))
                .addGap(0, 497, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(productDrop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(customerDrop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saleAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(saleQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3))
                .addContainerGap(275, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void productDropActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_productDropActionPerformed
        // TODO add your handling code here:
        String p = (String) productDrop.getSelectedItem();
    }//GEN-LAST:event_productDropActionPerformed

    public Product getProductObject(String val) {
        for (Product p : productList.getProductList()) {
            if (p.getProductName().equalsIgnoreCase(val)) {
                return p;
            }
        }
        return null;
    }

    public Customer getCustomerObject(String val) {
        for (Customer c : customerList.getCustomerList()) {
            if (c.getCustomerName().equalsIgnoreCase(val)) {
                return c;
            }
        }
        return null;
    }

    private void saveOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveOrderActionPerformed
        // TODO add your handling code here:
        int saleAmountVal = 0;
        int saleQuantityVal = 0;
       
        int selectedIndexProduct = productDrop.getSelectedIndex();
        int selectedIndexCustomer = customerDrop.getSelectedIndex();
        if(!saleAmount.getText().trim().equalsIgnoreCase("")) {
            saleAmountVal = Integer.parseInt(saleAmount.getText());
        }
        if(!saleQuantity.getText().trim().equalsIgnoreCase("")) {
            saleQuantityVal = Integer.parseInt(saleQuantity.getText());
        }

        if (saleAmountVal == 0) {
            JOptionPane.showMessageDialog(null, "Please enter sale amount", "Error", JOptionPane.ERROR_MESSAGE);
        }

        else if (saleQuantityVal == 0) {
            JOptionPane.showMessageDialog(null, "Please enter quantity of purchase", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (selectedIndexProduct == 0) {
            JOptionPane.showMessageDialog(null, "Please select a product", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (selectedIndexCustomer == 0) {
            JOptionPane.showMessageDialog(null, "Please select a customer", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            String productName = (String) productDrop.getSelectedItem();
            String customerName = (String) customerDrop.getSelectedItem();

            Product p = getProductObject(productName);
            Customer c = getCustomerObject(customerName);

            Order order = new Order();
            order = ordersList.addOrder();
            order.setCustomer(c);
            order.setProduct(p);
            order.setOrderAmount(saleAmountVal);
            order.setOrderQuantity(saleQuantityVal);
            order.setSalesRepName(LoginMain.loggedUser);

            JOptionPane.showMessageDialog(null, "Order created successfully.", "Success", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_saveOrderActionPerformed

    private void customerDropActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customerDropActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_customerDropActionPerformed

    private void saleQuantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saleQuantityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_saleQuantityActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add yopur handling code here:
        mainProcessContainer.remove(this);
        CardLayout layout = (CardLayout) mainProcessContainer.getLayout();
        layout.previous(mainProcessContainer);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void saleAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_saleAmountKeyTyped
       char enter = evt.getKeyChar();
        if(!(Character.isDigit(enter))){
            evt.consume();
            JOptionPane.showMessageDialog(null, "Enter numbers only");
        }
    }//GEN-LAST:event_saleAmountKeyTyped

    private void saleQuantityKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_saleQuantityKeyTyped
       char enter = evt.getKeyChar();
        if(!(Character.isDigit(enter))){
            evt.consume();
            JOptionPane.showMessageDialog(null, "Enter numbers only");
        }
    }//GEN-LAST:event_saleQuantityKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> customerDrop;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JComboBox<String> productDrop;
    private javax.swing.JTextField saleAmount;
    private javax.swing.JTextField saleQuantity;
    private javax.swing.JButton saveOrder;
    // End of variables declaration//GEN-END:variables
}
