/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author user
 */
public class Product {

    private String productID;
    private String productName;
    private int floorVal;
    private int actualVal;
    private int ceilVal;
    private String supplierName;
    private Market market;

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getFloorVal() {
        return floorVal;
    }

    public void setFloorVal(int floorVal) {
        this.floorVal = floorVal;
    }

    public int getActualVal() {
        return actualVal;
    }

    public void setActualVal(int actualVal) {
        this.actualVal = actualVal;
    }

    public int getCeilVal() {
        return ceilVal;
    }

    public void setCeilVal(int ceilVal) {
        this.ceilVal = ceilVal;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @Override
    public String toString() {
        return getProductName();
    }
}
