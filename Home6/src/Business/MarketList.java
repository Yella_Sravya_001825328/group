/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class MarketList {
    private ArrayList<Market> marketList;
    
    public MarketList() {
        marketList = new ArrayList<Market>();
    }

    public ArrayList<Market> getMarketList() {
        return marketList;
    }
    
    public Market addMarket() {
        Market m = new Market();
        marketList.add(m);
        return m;
    }
    
    public void deleteMarket(Market m) {
        marketList.remove(m);
    }
    
    
}
