/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author user
 */
public class Customer {
    private String customerName;
    private String customerAge;
    private String customerGender;
    private String customerMarketType;

    public String getCustomerMarketType() {
        return customerMarketType;
    }

    public void setCustomerMarketType(String marketType) {
        this.customerMarketType = marketType;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAge() {
        return customerAge;
    }

    public void setCustomerAge(String customerAge) {
        this.customerAge = customerAge;
    }

    public String getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(String customerGender) {
        this.customerGender = customerGender;
    }
    
    @Override
    public String toString() {
        return getCustomerName();
    }
}
