/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Analytics.*;
import java.sql.DatabaseMetaData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import static javafx.scene.input.KeyCode.T;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gauri Y
 */
public class AnalyticsPanel extends javax.swing.JPanel {

    private JPanel mainProcessContainer;
    private Business business;
    private MarketList marketList;
    private OrderList ordersList;
    private ProductList productList;
    private SalesPersonByMarketList salesPersonMarketList;
    private SalesPersonByMarketList productMarketList;
    private int revenueCounter = 0;
    private int rowId = 0;
    private int marketDisplayCounter = 0;
    private List<SalesPersonByMarket> opList;
    private List<SalesPersonByMarket> productsAboveTarget;

    /**
     * Creates new form AnalyticsPanel
     */
    public AnalyticsPanel(JPanel mainProcessContainer, Business business, MarketList marketList, OrderList ordersList, ProductList productList) {
        initComponents();
        this.mainProcessContainer = mainProcessContainer;
        this.business = business;
        this.marketList = marketList;
        this.ordersList = ordersList;
        this.productList = productList;
        salesPersonMarketList = new SalesPersonByMarketList();
        productMarketList = new SalesPersonByMarketList();
        loadMainData();
        loadMarketRevenueData();
        loadSalesTargetData();
        loadTopProductData();
    }

    public void loadMainData() {
        for (Market m : marketList.getMarketList()) {
            marketSelectDrop.addItem(m.getMarket());
        }

        for (Market m : marketList.getMarketList()) {
            topProductDrop.addItem(m.getMarket());
        }

        SalesPersonByMarket sp = new SalesPersonByMarket();

        for (Order o : ordersList.getOrderList()) {
            sp = salesPersonMarketList.addData();
            sp.setRowID(rowId++);
            sp.setSalesPersonName(o.getSalesRepName());
            sp.setProductName(o.getProduct().getProductName());
            sp.setMarketName(o.getProduct().getMarket().getMarket());

            int amount = o.getOrderAmount();
            int quantity = o.getOrderQuantity();
            int repRevenue = (amount * quantity);
            sp.setSalesPersonRevenue(repRevenue);
            revenueCounter += repRevenue;

            sp.setTargetSaleValue(o.getProduct().getActualVal() * quantity);
        }

        opList = new ArrayList<>();
        for (SalesPersonByMarket sm : salesPersonMarketList.getSalesMarketList()) {
            opList.add(sm);
        }

        // SORT THE LIST BY REVENUE FIRST
        Collections.sort(opList, new CompareByRevenue());
        // THEN SORT THE LIST BY MARKET
        Collections.sort(opList, CompareByMarket.marketComparator);

        DefaultTableModel dtm = (DefaultTableModel) marketTopTable.getModel();
        dtm.setRowCount(0);
        for (int i = 0; i < opList.size(); i++) {
            Object row[] = new Object[3];
            row[0] = opList.get(i).getSalesPersonName();
            row[1] = opList.get(i).getSalesPersonRevenue();
            row[2] = opList.get(i).getMarketName();
            dtm.addRow(row);
        }
        lblRevenueTotForXerox.setText(Integer.toString(revenueCounter));
    }

    public void loadMarketRevenueData() {
        String marketName = "";
        String oldMarketName = "";
        DefaultTableModel dtm = (DefaultTableModel) marketRevenueTable.getModel();
        dtm.setRowCount(0);
        int saleTotal = 0;
        for (int i = 0; i < opList.size(); i++) {
            marketName = opList.get(i).getMarketName();
            if (oldMarketName.equals("")) {
                // Indicates first iteration
                saleTotal += opList.get(i).getSalesPersonRevenue();
                oldMarketName = opList.get(i).getMarketName();
            } else {
                // Indicates iteration greater than 1
                if (marketName.equals(oldMarketName)) {
                    // Indicates same market
                    saleTotal += opList.get(i).getSalesPersonRevenue();
                    oldMarketName = opList.get(i).getMarketName();
                } else {
                    // Indicates there is a change in market.
                    // Reset the saleTotal amount to 0.
                    Object row[] = new Object[2];
                    row[0] = oldMarketName;
                    row[1] = saleTotal;
                    dtm.addRow(row);
                    saleTotal = 0;
                    oldMarketName = opList.get(i).getMarketName();
                }
            }
        }
        Object row[] = new Object[2];
        row[0] = oldMarketName;
        row[1] = saleTotal;
        dtm.addRow(row);
    }

    public void loadSalesTargetData() {
        ArrayList<String> mainArr = new ArrayList<>();

        Set<String> setSalesRepName = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
        for (SalesPersonByMarket sm : salesPersonMarketList.getSalesMarketList()) {
            setSalesRepName.add(sm.getSalesPersonName());
        }
        int productSum = 0;
        int sellingPriceSum = 0;
        mainArr = new ArrayList<>(setSalesRepName);
        DefaultTableModel dtm = (DefaultTableModel) aboveTargetTable.getModel();
        dtm.setRowCount(0);

        DefaultTableModel dtm1 = (DefaultTableModel) belowTargetTable.getModel();
        dtm1.setRowCount(0);
        for (int i = 0; i < mainArr.size(); i++) {
            for (Order or : ordersList.getOrderList()) {
                if (or.getSalesRepName().equalsIgnoreCase(mainArr.get(i))) {
                    // Perform further checks
                    productSum += or.getProduct().getActualVal() * or.getOrderQuantity();
                    sellingPriceSum += or.getOrderAmount() * or.getOrderQuantity();
                }
            }
            if (sellingPriceSum > productSum) {
                Object row[] = new Object[3];
                row[0] = mainArr.get(i);
                row[1] = productSum;
                row[2] = sellingPriceSum;
                dtm.addRow(row);
            } else if (sellingPriceSum < productSum) {
                Object row1[] = new Object[3];
                row1[0] = mainArr.get(i);
                row1[1] = productSum;
                row1[2] = sellingPriceSum;
                dtm1.addRow(row1);
            }
            productSum = 0;
            sellingPriceSum = 0;
        }
    }

    public void loadTopProductData() {
        SalesPersonByMarket sp = new SalesPersonByMarket();

        for (Order o : ordersList.getOrderList()) {
            int amount = o.getOrderAmount();
            int quantity = o.getOrderQuantity();

            // Calculate the revenue collected
            int repRevenue = (amount * quantity);

            // Calculate the actual target proce expected from sale
            int target = o.getProduct().getActualVal();
            int productActualTarget = target * quantity;
            if (repRevenue > productActualTarget) {
                sp = productMarketList.addData();
                sp.setRowID(rowId++);
                sp.setSalesPersonName(o.getSalesRepName());
                sp.setProductName(o.getProduct().getProductName());
                sp.setMarketName(o.getProduct().getMarket().getMarket());
                sp.setSalesPersonRevenue(repRevenue);
                sp.setTargetSaleValue(productActualTarget);
            }
        }

        productsAboveTarget = new ArrayList<>();
        for (SalesPersonByMarket sm : productMarketList.getSalesMarketList()) {
            productsAboveTarget.add(sm);
        }
        Collections.sort(productsAboveTarget, new CompareByRevenue());
        Collections.sort(productsAboveTarget, new CompareByTargetValue());

        DefaultTableModel dtm = (DefaultTableModel) topProductsTable.getModel();
        dtm.setRowCount(0);
        for (int i = 0; i < productsAboveTarget.size(); i++) {
            Object row[] = new Object[4];
            row[0] = productsAboveTarget.get(i).getProductName();
            row[1] = productsAboveTarget.get(i).getMarketName();
            row[2] = productsAboveTarget.get(i).getTargetSaleValue();
            row[3] = productsAboveTarget.get(i).getSalesPersonRevenue();
            dtm.addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        marketTopPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        marketTopTable = new javax.swing.JTable();
        lblRevenueTotForXerox = new javax.swing.JLabel();
        marketSelectDrop = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        aboveTargetTable = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        belowTargetTable = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        marketRevenueTable = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        topProductsTable = new javax.swing.JTable();
        topProductDrop = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();

        marketTopPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Top Sales Persons By Market", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 18))); // NOI18N

        marketTopTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sales Person", "Revenue By Sales", "Market"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(marketTopTable);
        if (marketTopTable.getColumnModel().getColumnCount() > 0) {
            marketTopTable.getColumnModel().getColumn(0).setResizable(false);
            marketTopTable.getColumnModel().getColumn(1).setResizable(false);
            marketTopTable.getColumnModel().getColumn(2).setResizable(false);
        }

        javax.swing.GroupLayout marketTopPanelLayout = new javax.swing.GroupLayout(marketTopPanel);
        marketTopPanel.setLayout(marketTopPanelLayout);
        marketTopPanelLayout.setHorizontalGroup(
            marketTopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(marketTopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 680, Short.MAX_VALUE)
                .addContainerGap())
        );
        marketTopPanelLayout.setVerticalGroup(
            marketTopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(marketTopPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                .addContainerGap())
        );

        marketSelectDrop.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--SELECT--" }));
        marketSelectDrop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                marketSelectDropActionPerformed(evt);
            }
        });

        jLabel1.setText("SELECT MARKET FILTER");

        aboveTargetTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sales Person Name", "Sales Target (By sold products)", "Actual Sales"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(aboveTargetTable);
        if (aboveTargetTable.getColumnModel().getColumnCount() > 0) {
            aboveTargetTable.getColumnModel().getColumn(0).setResizable(false);
            aboveTargetTable.getColumnModel().getColumn(1).setResizable(false);
            aboveTargetTable.getColumnModel().getColumn(2).setResizable(false);
        }

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        belowTargetTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Sales Person Name", "Sales Target (By sold products)", "Actual Sales"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(belowTargetTable);
        if (belowTargetTable.getColumnModel().getColumnCount() > 0) {
            belowTargetTable.getColumnModel().getColumn(0).setResizable(false);
            belowTargetTable.getColumnModel().getColumn(1).setResizable(false);
            belowTargetTable.getColumnModel().getColumn(2).setResizable(false);
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 538, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        marketRevenueTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Market Name", "Total Revenue"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane4.setViewportView(marketRevenueTable);
        if (marketRevenueTable.getColumnModel().getColumnCount() > 0) {
            marketRevenueTable.getColumnModel().getColumn(0).setResizable(false);
            marketRevenueTable.getColumnModel().getColumn(1).setResizable(false);
        }

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 122, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(0, 51, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        topProductsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product Name", "Market", "Target Revenue", "Actual Revenue"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(topProductsTable);
        if (topProductsTable.getColumnModel().getColumnCount() > 0) {
            topProductsTable.getColumnModel().getColumn(0).setResizable(false);
            topProductsTable.getColumnModel().getColumn(1).setResizable(false);
            topProductsTable.getColumnModel().getColumn(2).setResizable(false);
            topProductsTable.getColumnModel().getColumn(3).setResizable(false);
        }

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        topProductDrop.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECT --" }));
        topProductDrop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                topProductDropActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Verdana", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(153, 0, 153));
        jLabel2.setText("TOTAL COMPANY REVENUE BY ORDERS");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(80, 80, 80)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(503, 503, 503)
                                .addComponent(lblRevenueTotForXerox, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(marketTopPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addGap(18, 18, 18)
                                        .addComponent(marketSelectDrop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(18, 18, 18)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(304, 304, 304)
                                        .addComponent(topProductDrop, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(jLabel2)))
                .addContainerGap(663, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(lblRevenueTotForXerox, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(88, 88, 88)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(marketSelectDrop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(marketTopPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(topProductDrop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(240, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void marketSelectDropActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_marketSelectDropActionPerformed
        // TODO add your handling code here:
        DefaultTableModel dtm = (DefaultTableModel) marketTopTable.getModel();
        dtm.setRowCount(0);
        String item = (String) marketSelectDrop.getSelectedItem();
        if (marketSelectDrop.getSelectedIndex() != 0) {
            for (int i = 0; i < opList.size(); i++) {
                if (opList.get(i).getMarketName().equalsIgnoreCase(item)) {
                    if (marketDisplayCounter < 10) {
                        Object row[] = new Object[4];
                        row[0] = opList.get(i);
                        row[1] = opList.get(i).getSalesPersonName();
                        row[2] = opList.get(i).getSalesPersonRevenue();
                        row[3] = opList.get(i).getMarketName();
                        dtm.addRow(row);
                        marketDisplayCounter++;
                    }
                }
            }
            marketDisplayCounter = 0;
        } else {
            for (int i = 0; i < opList.size(); i++) {
                Object row[] = new Object[4];
                row[0] = opList.get(i);
                row[1] = opList.get(i).getSalesPersonName();
                row[2] = opList.get(i).getSalesPersonRevenue();
                row[3] = opList.get(i).getMarketName();
                dtm.addRow(row);
            }
            marketDisplayCounter = 0;
        }
    }//GEN-LAST:event_marketSelectDropActionPerformed

    private void topProductDropActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_topProductDropActionPerformed
        // TODO add your handling code here:
        DefaultTableModel dtm = (DefaultTableModel) topProductsTable.getModel();
        dtm.setRowCount(0);
        marketDisplayCounter = 0;
        String item = (String) topProductDrop.getSelectedItem();
        if (topProductDrop.getSelectedIndex() != 0) {
            for (int i = 0; i < productsAboveTarget.size(); i++) {
                if (productsAboveTarget.get(i).getMarketName().equalsIgnoreCase(item)) {
                    if (marketDisplayCounter < 3) {
                        Object row[] = new Object[4];
                        row[0] = productsAboveTarget.get(i).getProductName();
                        row[1] = productsAboveTarget.get(i).getMarketName();
                        row[2] = productsAboveTarget.get(i).getTargetSaleValue();
                        row[3] = productsAboveTarget.get(i).getSalesPersonRevenue();
                        dtm.addRow(row);
                        marketDisplayCounter++;
                    }
                }
            }
            marketDisplayCounter = 0;
        } else {
            for (int i = 0; i < productsAboveTarget.size(); i++) {
                Object row[] = new Object[4];
                row[0] = productsAboveTarget.get(i).getProductName();
                row[1] = productsAboveTarget.get(i).getMarketName();
                row[2] = productsAboveTarget.get(i).getTargetSaleValue();
                row[3] = productsAboveTarget.get(i).getSalesPersonRevenue();
                dtm.addRow(row);
            }
            marketDisplayCounter = 0;
        }

    }//GEN-LAST:event_topProductDropActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable aboveTargetTable;
    private javax.swing.JTable belowTargetTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel lblRevenueTotForXerox;
    private javax.swing.JTable marketRevenueTable;
    private javax.swing.JComboBox<String> marketSelectDrop;
    private javax.swing.JPanel marketTopPanel;
    private javax.swing.JTable marketTopTable;
    private javax.swing.JComboBox<String> topProductDrop;
    private javax.swing.JTable topProductsTable;
    // End of variables declaration//GEN-END:variables
}
