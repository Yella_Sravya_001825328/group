/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analytics;
import Business.*;
import Analytics.*;
import java.util.Comparator;
/**
 *
 * @author Gauri Y
 */
public class CompareByRevenue implements Comparator<SalesPersonByMarket>{
    
    @Override
    public int compare(SalesPersonByMarket t, SalesPersonByMarket t1) {
        if(t.getSalesPersonRevenue() < t1.getSalesPersonRevenue()) {
            return 1;
        } else {
            return -1;
        }
    }    
}
